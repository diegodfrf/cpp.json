//
//    Created by Diego Fernando Rodriguez Fuentes <diegodfrf@gmail.com> on 13/11/19.
//    Copyright (c) All rights reserved.
//
//    MIT License
//
//    Copyright (c) 2019 Diego Fernando Rodriguez
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

#include <iostream>
#include <string>
#include <cppjson.hpp>

int main()
{
    json::JSON obj;
    obj["Token"]              = "ABCDE";
    obj["Num"]                = 1;
    obj["Telefonos"][0]       = 6111111;
    obj["Telefonos"][1]       = 4333333;
    obj["Paises"]["Colombia"] = "Bogota";
    obj["Paises"]["Crancia"]  = "Paris";

    std::cout << obj << std::endl;

    std::cout << "================================" << std::endl;

    std::stringstream input;
    input << obj;

    std::string myJson = obj.str();
    std::cout << myJson << std::endl;

    std::cout << "================================" << std::endl;

    std::string myJsonRaw = R"(
        {
            "glossary": {
                "title": "example glossary",
                "GlossDiv": {
                    "title": "S",
                    "GlossList": {
                        "GlossEntry": {
                            "ID": "SGML",
                            "SortAs": "SGML",
                            "GlossTerm": "Standard Generalized Markup Language",
                            "Acronym": "SGML",
                            "Abbrev": "ISO 8879:1986",
                            "GlossDef": {
                                "para": "A meta-markup language, used to create markup languages such as DocBook.",
                                "GlossSeeAlso": ["GML", "XML"]
                            },
                            "GlossSee": "markup"
                        }
                    }
                }
            }
        }
    )";

    json::JSON myJson2 = json::JSON::Load( myJsonRaw );
    std::cout << myJson2 << std::endl;

    std::cout << "================================" << std::endl;

    json::JSON obj2;
    obj2["array"]                      = json::Array( true, "Two", 3, 4.0 );
    obj2["obj2"]                       = json::Object();
    obj2["obj2"]["inner"]              = "Inside";
    obj2["new"]["some"]["deep"]["key"] = "Value";
    obj2["array2"].append( false, "three" );

    std::cout << obj2 << std::endl;

    std::cout << "================================" << std::endl;

    json::JSON obj3 = { "array", json::Array( true, "Two", 3, 4.0 ),         "obj",    { "inner", "Inside" },
                        "new",   { "some", { "deep", { "key", "Value" } } }, "array2", json::Array( false, "three" ) };

    std::cout << obj3 << std::endl;

    std::cout << "================================" << std::endl;

    std::cout << obj3.strUgly() << std::endl;

    std::cout << "================================" << std::endl;
}
